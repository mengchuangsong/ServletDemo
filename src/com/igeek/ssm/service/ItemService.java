/**
 * 
 */
package com.igeek.ssm.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.igeek.ssm.po.Item;

/**
 * @author Administrator
 *
 */
@Service
public interface ItemService {

	public List<Item> queryItemAll();
	
	public Item queryItem(int id);
	
	public int insertItem(Item item);
	
	public int updateItem(Item item);
	
	public int deleteItem(int id);

}
