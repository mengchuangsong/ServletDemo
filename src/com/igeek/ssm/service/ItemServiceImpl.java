/**
 * 
 */
package com.igeek.ssm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igeek.ssm.mapper.ItemMapper;
import com.igeek.ssm.po.Item;

/**
 * @author Administrator
 *
 */
@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemMapper itemMapper;
	
	@Override
	public List<Item> queryItemAll() {
		// TODO Auto-generated method stub
		return itemMapper.selectAll();
	}

	@Override
	public Item queryItem(int id) {
		// TODO Auto-generated method stub
		return itemMapper.selectByPrimaryKey(id);
	}

	@Override
	public int insertItem(Item item) {
		// TODO Auto-generated method stub
		return itemMapper.insert(item);
	}

	@Override
	public int updateItem(Item item) {
		// TODO Auto-generated method stub
		return itemMapper.updateByPrimaryKey(item);
	}

	@Override
	public int deleteItem(int id) {
		// TODO Auto-generated method stub
		return itemMapper.deleteByPrimaryKey(id);
	}
	  

}
