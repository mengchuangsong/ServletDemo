/**
 * 
 */
package com.igeek.ssm.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.igeek.ssm.po.Item;
import com.igeek.ssm.service.ItemService;

/**
 * @author Administrator
 *
 */
@Controller
public class ItemController {

	
	  @Autowired
	  private ItemService itemService;
	 
	
	@RequestMapping(value="/itemList", method=RequestMethod.GET)
	public ModelAndView queryItemList(ModelAndView modelAndView)
	{
		List<Item> items =  itemService.queryItemAll();
		modelAndView.addObject("itemList", items);
		modelAndView.setViewName("itemList");
		return modelAndView;
	}
	/***
	 * 除了ModelAndView 以外,还可以使用Model来向页面传递数据.
	 * Model 是一个接口,在参数里面直接声明Model 即可
	 * 
	 * 如果使用Model则可以不使用ModelAndView对象,Model对象可以向页面传递数据
	 * ,view 对象则可以使用String返回值代替
	 * 不管是Model还是ModelAndView,其本质都是使用request对象向jsp传递数据
	 */
//	
//	方式一: 使用 ModelAndView 
//	@RequestMapping("itemEdit")
//	public ModelAndView queryById(HttpServletRequest reqeust)
//	{
//		String id = reqeust.getParameter("id");
//		Item item = itemService.queryItem(Integer.valueOf(id));
//		ModelAndView modelAndView = new ModelAndView();
//		modelAndView.addObject("item",item);
//		modelAndView.setViewName("itemEdit");
//		return modelAndView;
//	}
//	方式二: 使用Model
//	@RequestMapping("itemEdit")
//	public String queryById(HttpServletRequest request ,Model model)
//	{
//		String id = request.getParameter("id");
//		Item item = itemService.queryItem(Integer.valueOf(id));
//		model.addAttribute("item",item);
//		return "itemEdit";
//	}
	
//	方式三: 使用ModelMap
//	@RequestMapping("itemEdit")
//	public String queryById(HttpServletRequest request,ModelMap modelMap)
//	{
//		String id = request.getParameter("id");
//		Item item = itemService.queryItem(Integer.valueOf(id));
//		modelMap.addAttribute("item",item);
//		return "itemEdit";
//	}
	
//	绑定简单类型
//	@RequestMapping("itemEdit")
//	public String queryById(int id,ModelMap modelMap)
//	{
//		Item item = itemService.queryItem(id);
//		modelMap.addAttribute("item", item);
//		return "itemEdit";
//	}
	
//	简单类型的绑定
	@RequestMapping("itemEdit")
	public String queryById(@RequestParam(value="id",required=true,defaultValue="1")Integer id, ModelMap modelMap)
	{
		Item item = itemService.queryItem(id);
		modelMap.addAttribute("item", item);
		return "itemEdit";
	}
	
	
	@RequestMapping("updateItem")
	public String updateItem(HttpServletRequest request)
	{
		String idParams = request.getParameter("id");
		String nameParams = request.getParameter("name");
		String priceParams  = request.getParameter("price");
		String createtimeParams = request.getParameter("createtime");
		String detail = request.getParameter("detail");
		Item item = new Item();
		item.setId(Integer.valueOf(idParams));
		item.setPrice(Float.valueOf(priceParams));
		item.setName(nameParams);
		item.setDetail(detail);
		try {
			item.setCreatetime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(createtimeParams));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int column = itemService.updateItem(item);
		System.out.println("变更" + column + "条数据!");
		return "itemList";
	}
	
	@RequestMapping("/queryItem")
	public ModelAndView queryItem(Item item,HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		String ids = request.getParameter("id");
		if(ids.isEmpty())
		{
			List<Item> items =  itemService.queryItemAll();
			modelAndView.addObject("itemList", items);
			modelAndView.setViewName("itemList");
		}
		else
		{
			int id = Integer.valueOf(ids);
			Item it =  itemService.queryItem(id);
			List<Item> items = new ArrayList<Item>();
			items.add(it);
			modelAndView.addObject("itemList", items);
			modelAndView.setViewName("itemList");
			System.out.println("item: "+ it.getId()+ "\t" + it.getName() + "\t" + it.getPrice() + "\t" + it.getDetail());
		}
		return modelAndView;
	}
	
	
}
