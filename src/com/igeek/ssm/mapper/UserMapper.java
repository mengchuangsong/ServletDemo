package com.igeek.ssm.mapper;

import com.igeek.ssm.po.User;
import java.util.List;

public interface UserMapper {
    int insert(User record);

    List<User> selectAll();
}