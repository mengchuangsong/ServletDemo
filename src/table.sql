drop database if exists goods;
create database goods default charset set utf8;
use goods;
create table user(
id integer primary key auto_increment,
name varchar(32) not null,
sex varchar(1) ,
address varchar(255),
birthday date
) engine = innodb;
create table item(
id integer primary key auto_increment,
name varchar(32) not null,
price float(10,1),
pic varchar(64),
createtime datetime,
detail text
)engine = innodb;

insert into item
values(null,'台式机',3000.0,'','2016-02-09 21:09:32','国庆大促销,快来购买!!');
insert into item
values(null,'笔记本',5000.0,'','2017-05-02 08:09:32','高端配置笔记本,最新操作系统欢迎您来购买!!');
insert into item
values(null,'背包',200.0,'','2018-02-09 21:09:32','多功能旅行包,满足你的所有需求!!');

