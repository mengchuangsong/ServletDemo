<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>修改商品信息</title>
</head>
<body>
<!-- ${pageContext.request.contextPath}/updateItem -->
<form id="itemForm" action="/SpringMybits/updateItem" method="GET">
<input type="hidden" name="id" value="${item.id }"/> 修改商品信息
<table width="100%" border=1 >
<tr>
	<td>商品名称</td>
	<td> <input type="text" name="name"  value="${item.name }"/></td>
</tr>

<tr>
	<td>商品价格</td>
	<td><input type="text" name="price"  value="${item.price }"/></td>

</tr>

<tr>
	<td>生产日期</td>
	<td>
		<input type="text" name="createtime" 
			value="<fmt:formatDate value="${item.createtime}" pattern="yyyy-MM-dd HH:mm:ss" />">
			
	 </td>
</tr>

<tr>
	<td>商品描述</td>
	<td><input type="text" name="detail" value="${item.detail }"/></td>
</tr>

<tr >
	<td align="center" valign="middle" colspan ="2"><input type="submit" 
	 value="修改" /></td>
	
 </tr>



</table>


</form>

<script type="text/javascript">

	function onUpdate()
	{
		var inputLabel = document.getElementsByTagName("input");
		var names = inputLabel[1].value;
		var prices = inputLabel[2].value;
		var createtimes = inputLabel[3].value;
		var details = inputLabel[4].value;

		console.log("name:"+names);
		console.log("price:" + prices);
		console.log("createtime:"+ createtimes);
		console.log("detail:" + details);
		
		//window.location.href="/SpringMybits/updateItem?id=${item.id}&name=${names}&price=${prices}&createtime=${createtimes}&detail=${details}";
	}
	

</script>

</body>
</html>